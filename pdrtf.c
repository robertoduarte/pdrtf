#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <pthread.h>
#include <sched.h>
#include <stdio.h>
#include <signal.h>

#include "cluster_configuration.h"
#include "pdrtf.h"
#include "real_time_utils.h"
#include "ftt_se_wrapper.h"

void print_error(char * function_name, int return_value) {
    printf("\nError. Function %s() returned with value: %d.", function_name, return_value);
}


unsigned char pdrtf_init_state = 0;
unsigned char pdrtf_launch_state = 0;

int pdrtf_get_local_node_type(){
    return get_local_node_type();
}

typedef struct pdrtf_node {
    unsigned char active;
    void * function;
    pthread_t thread_id;

    void * input_data_ptr;
    unsigned short input_data_size;
    stream * input_stream;

    void * output_data_ptr;
    unsigned short output_data_size;
    stream * output_stream;

} pdrtf_node;

pdrtf_node * allocate_pdrtf_node() {
    pdrtf_node * new_pdrtf_node;
    if ((new_pdrtf_node = calloc(1, sizeof (pdrtf_node))) == 0) {
        return 0;
    }
    new_pdrtf_node->active = 0;
    new_pdrtf_node->function = 0;
    new_pdrtf_node->thread_id = 0;
    new_pdrtf_node->input_data_size = 0;
    new_pdrtf_node->output_data_size = 0;
    new_pdrtf_node->input_stream = 0;
    new_pdrtf_node->output_stream = 0;
    return new_pdrtf_node;
};

typedef struct pdrtf_task {
    void * function;
    pthread_t thread_id;
    unsigned short period_in_ms;
    unsigned short linux_priority;
    pdrtf_node ** pdrtf_node_array;
    unsigned short number_of_pdrtf_nodes;
    unsigned short number_of_active_pdrtf_nodes;
} pdrtf_task;

pdrtf_task * allocate_pdrtf_task() {

    pdrtf_task * new_pdrtf_task;
    if ((new_pdrtf_task = calloc(1, sizeof (pdrtf_task))) == 0) {
        return 0;
    }

    new_pdrtf_task->function = 0;
    new_pdrtf_task->thread_id = 0;
    new_pdrtf_task->period_in_ms = 0;
    new_pdrtf_task->linux_priority = 0;
    new_pdrtf_task->pdrtf_node_array = 0;
    new_pdrtf_task->number_of_pdrtf_nodes = 0;
    new_pdrtf_task->number_of_active_pdrtf_nodes = 0;
    return new_pdrtf_task;
}

pdrtf_task * * pdrtf_task_array;

unsigned short number_of_pdrtf_tasks = 0;

int new_pdrtf_task(void * function_ptr, unsigned short period_in_ms, unsigned short linux_priority) {

    if (pdrtf_init_state != 1) {
        return -1;
    }
    if (pdrtf_launch_state != 0) {
        return -2;
    }
    if (function_ptr == 0) {
        return -3;
    }
    if (linux_priority > 48) {
        return -4;
    }
    if (number_of_pdrtf_tasks == 0) {

        if ((pdrtf_task_array = calloc(1, sizeof (pdrtf_task *))) == 0) {
            return -6;
        }

        if ((pdrtf_task_array[0] = allocate_pdrtf_task()) == 0) {
            return -7;
        }
        number_of_pdrtf_tasks++;
        pdrtf_task_array[0]->function = function_ptr;
        pdrtf_task_array[0]->period_in_ms = period_in_ms;
        pdrtf_task_array[0]->linux_priority = linux_priority;

        pdrtf_task_array[0]->pdrtf_node_array = calloc(get_number_of_remote_nodes(), sizeof (pdrtf_node*));
        int i;
        for (i = 0; i < get_number_of_remote_nodes(); i++) {
            if ((pdrtf_task_array[0]->pdrtf_node_array[i] = allocate_pdrtf_node()) == 0) {
                return -8;
            }
        }
        return number_of_pdrtf_tasks;

    } else {
        if (realloc(pdrtf_task_array, (number_of_pdrtf_tasks + 1) * sizeof (pdrtf_task *)) == 0) {
            return -9;
        }
        if ((pdrtf_task_array[number_of_pdrtf_tasks] = allocate_pdrtf_task()) == 0) {
            return -10;
        }

        pdrtf_task_array[number_of_pdrtf_tasks]->function = function_ptr;
        pdrtf_task_array[number_of_pdrtf_tasks]->period_in_ms = period_in_ms;
        pdrtf_task_array[number_of_pdrtf_tasks]->linux_priority = linux_priority;

        if ((pdrtf_task_array[number_of_pdrtf_tasks]->pdrtf_node_array = calloc(get_number_of_remote_nodes(), sizeof (pdrtf_node*))) == 0) {
            return -11;
        }
        int i;
        for (i = 0; i < get_number_of_remote_nodes(); i++) {
            if ((pdrtf_task_array[number_of_pdrtf_tasks]->pdrtf_node_array[i] = allocate_pdrtf_node()) == 0) {
                return -12;
            }
        }
        number_of_pdrtf_tasks++;
        return number_of_pdrtf_tasks;
    }


}

int set_pdrtf_task_sub_task(unsigned short pdrtf_task_id, unsigned short node_id, void * function, unsigned short input_data_size, unsigned short output_data_size) {
    if (pdrtf_init_state != 1) {
        return -1;
    }
    if (pdrtf_launch_state != 0) {
        return -2;
    }
    if (pdrtf_task_id == 0 || pdrtf_task_id > number_of_pdrtf_tasks) {
        return -3;
    }

    if (node_id == 0 || node_id > get_number_of_remote_nodes()) {
        return -4;
    }
    if (function == 0) {
        return -5;
    }


    pdrtf_task_array[pdrtf_task_id - 1]->pdrtf_node_array[node_id - 1]->function = function;
    pdrtf_task_array[pdrtf_task_id - 1]->pdrtf_node_array[node_id - 1]->input_data_size = input_data_size;
    pdrtf_task_array[pdrtf_task_id - 1]->pdrtf_node_array[node_id - 1]->output_data_size = output_data_size;
    pdrtf_task_array[pdrtf_task_id - 1]->pdrtf_node_array[node_id - 1]->input_data_ptr = calloc(1, input_data_size);
    pdrtf_task_array[pdrtf_task_id - 1]->pdrtf_node_array[node_id - 1]->output_data_ptr = calloc(1, output_data_size);

    if (pdrtf_task_array[pdrtf_task_id - 1]->pdrtf_node_array[node_id - 1]->active == 0) {
        pdrtf_task_array[pdrtf_task_id - 1]->number_of_active_pdrtf_nodes++;
        pdrtf_task_array[pdrtf_task_id - 1]->pdrtf_node_array[node_id - 1]->active = 1;
    }
    return 1;
}

int pdrtf_generate_stream_id(unsigned int pdrtf_task_id, unsigned int pdrtf_node_id) {
    return 1000 * pdrtf_task_id + 10 * pdrtf_node_id;
}

int pdrtf_open_task_node_streams(pdrtf_node * pdrtf_node_ptr, unsigned short pdrtf_task_id, unsigned short pdrtf_node_id) {
    int ret;

    if ((ret = ftt_se_wrapper_new_stream(&pdrtf_node_ptr->input_stream, pdrtf_generate_stream_id(pdrtf_task_id, pdrtf_node_id), 1, 1)) < 0) {
        print_error("ftt_se_wrapper_new_stream", ret);
        return -1;
    }

    if ((ret = ftt_se_wrapper_set_stream_parameters(pdrtf_node_ptr->input_stream, pdrtf_node_ptr->input_data_size, 1450, 2)) < 0) {
        print_error("ftt_se_wrapper_set_stream_parameters", ret);
        return -2;
    }
    if ((ret = ftt_se_wrapper_initialize_producer(pdrtf_node_ptr->input_stream)) < 0) {
        print_error("ftt_se_wrapper_initialize_producer", ret);
        return -3;
    }

    if ((ret = ftt_se_wrapper_new_stream(&pdrtf_node_ptr->output_stream, pdrtf_generate_stream_id(pdrtf_task_id, pdrtf_node_id) + 1, 1, 1)) < 0) {
        print_error("ftt_se_wrapper_new_stream", ret);
        return -4;
    }

    if ((ret = ftt_se_wrapper_set_stream_parameters(pdrtf_node_ptr->output_stream, pdrtf_node_ptr->output_data_size, 1450, 2)) < 0) {
        print_error("ftt_se_wrapper_set_stream_parameters", ret);
        return -5;
    }
    if ((ret = ftt_se_wrapper_initialize_consumer(pdrtf_node_ptr->output_stream)) < 0) {
        print_error("ftt_se_wrapper_initialize_consumer", ret);
        return -6;
    }
    return 1;
}

int pdrtf_open_remote_node_streams(pdrtf_node * pdrtf_node_ptr, unsigned short pdrtf_task_id, unsigned short pdrtf_node_id) {

    int ret;

    if ((ret = ftt_se_wrapper_new_stream(&pdrtf_node_ptr->input_stream, pdrtf_generate_stream_id(pdrtf_task_id, pdrtf_node_id), 1, 1)) < 0) {
        print_error("ftt_se_wrapper_new_stream", ret);
        return -1;
    }

    if ((ret = ftt_se_wrapper_initialize_consumer(pdrtf_node_ptr->input_stream)) < 0) {
        print_error("ftt_se_wrapper_initialize_consumer", ret);
        return -2;
    }

    if ((ret = ftt_se_wrapper_new_stream(&pdrtf_node_ptr->output_stream, pdrtf_generate_stream_id(pdrtf_task_id, pdrtf_node_id) + 1, 1, 1)) < 0) {
        print_error("ftt_se_wrapper_new_stream", ret);
        return -3;
    }

    if ((ret = ftt_se_wrapper_initialize_producer(pdrtf_node_ptr->output_stream)) < 0) {
        print_error("ftt_se_wrapper_initialize_producer", ret);
        return -4;
    }
    return 1;
}

int pdrtf_open_streams() {
    int task_index;
    if (get_local_node_type() < 0) {
        return -1;
    }
    if (get_local_node_type() == 1) {
        int node_index;
        for (task_index = 0; task_index < number_of_pdrtf_tasks; task_index++) {
            for (node_index = 0; node_index < get_number_of_remote_nodes(); node_index++) {
                if (pdrtf_task_array[task_index]->pdrtf_node_array[node_index]->active == 1) {
                    if ((pdrtf_open_task_node_streams(pdrtf_task_array[task_index]->pdrtf_node_array[node_index], task_index + 1, node_index + 1)) < 0) {
                        return -2;
                    }
                }
            }
        }
    } else {
        for (task_index = 0; task_index < number_of_pdrtf_tasks; task_index++) {
            if (pdrtf_task_array[task_index]->pdrtf_node_array[get_local_node_id()]->active == 1) {
                if ((pdrtf_open_remote_node_streams(pdrtf_task_array[task_index]->pdrtf_node_array[get_local_node_id()], task_index + 1, get_local_node_id() + 1)) < 0) {
                    return -3;
                }
            }
        }
    }
    return 1;
}

int pdrtf_send(void * msg_ptr, unsigned short msg_size, unsigned short pdrtf_task_id, unsigned short pdrtf_node_id) {
    stream * temp_stream;

    if (pdrtf_task_id < 0 || pdrtf_task_id >= number_of_pdrtf_tasks) {
        return -2;
    }
    if (pdrtf_node_id < 0 || pdrtf_node_id >= get_number_of_remote_nodes()) {
        return -3;
    }
    if (pdrtf_task_array[pdrtf_task_id]->pdrtf_node_array[pdrtf_node_id]->active == 0) {
        return -4;
    }

    if (get_local_node_type() == 1) {
        temp_stream = pdrtf_task_array[pdrtf_task_id]->pdrtf_node_array[pdrtf_node_id]->input_stream;
    } else {
        temp_stream = pdrtf_task_array[pdrtf_task_id]->pdrtf_node_array[pdrtf_node_id]->output_stream;
    }


    if (ftt_se_wrapper_send(temp_stream, msg_ptr, msg_size, 0) == 1) {
        return 1;
    } else {

        return -1;
    }

}

int pdrtf_receive(void * msg_ptr, unsigned short msg_size, unsigned short pdrtf_task_id, unsigned short pdrtf_node_id) {
    stream * temp_stream;

    if (pdrtf_task_id < 0 || pdrtf_task_id >= number_of_pdrtf_tasks) {
        return -2;
    }
    if (pdrtf_node_id < 0 || pdrtf_node_id >= get_number_of_remote_nodes()) {
        return -3;
    }
    if (pdrtf_task_array[pdrtf_task_id]->pdrtf_node_array[pdrtf_node_id]->active == 0) {
        return -4;
    }

    if (get_local_node_type() == 1) {
        temp_stream = pdrtf_task_array[pdrtf_task_id]->pdrtf_node_array[pdrtf_node_id]->output_stream;
    } else {
        temp_stream = pdrtf_task_array[pdrtf_task_id]->pdrtf_node_array[pdrtf_node_id]->input_stream;
    }

    int ret;
    if ((ret = ftt_se_wrapper_receive(temp_stream, msg_ptr, msg_size)) == 1) {
        return 1;
    } else {
        printf("ftt_se_wrapper_receive returned %d\n", ret);
        return -1;
    }
}

typedef struct pdrtf_thread_data {
    unsigned short pdrtf_task_id;
    unsigned short pdrtf_task_node_id;
    synchronous_start_param * ss_p;
} pdrtf_thread_data;

int pdrtf_get_input_parameters(void * task_data, void * data, unsigned short data_size) {
    if (pdrtf_init_state != 1) {
        return -1;
    }
    if (pdrtf_launch_state != 1) {
        return -2;
    }

    pdrtf_thread_data * pdrtf_thread_data_ptr = (pdrtf_thread_data*) task_data;

    if (pdrtf_thread_data_ptr == 0) {
        return -3;
    }
    if (data == 0) {
        return -4;
    }
    pdrtf_node * pdrtf_node_ptr = pdrtf_task_array[pdrtf_thread_data_ptr->pdrtf_task_id]->pdrtf_node_array[pdrtf_thread_data_ptr->pdrtf_task_node_id];

    if (data_size != pdrtf_node_ptr->output_data_size) {
        return -5;
    }

    memmove(data, pdrtf_node_ptr->input_data_ptr, data_size);
    return 1;


}

int pdrtf_set_output_parameters(void * task_data, void * data, unsigned short data_size) {
    if (pdrtf_init_state != 1) {
        return -1;
    }
    if (pdrtf_launch_state != 1) {
        return -2;
    }
    pdrtf_thread_data * pdrtf_thread_data_ptr = (pdrtf_thread_data*) task_data;

    if (pdrtf_thread_data_ptr == 0) {
        return -3;
    }
    if (data == 0) {
        return -4;
    }
    pdrtf_node * pdrtf_node_ptr = pdrtf_task_array[pdrtf_thread_data_ptr->pdrtf_task_id]->pdrtf_node_array[pdrtf_thread_data_ptr->pdrtf_task_node_id];

    if (data_size != pdrtf_node_ptr->output_data_size) {
        return -5;
    }

    memmove(pdrtf_node_ptr->output_data_ptr, data, data_size);
    return 1;

}

void* pdrtf_task_remote_thread(void * param) {

    pdrtf_thread_data * pdrtf_thread_data_ptr = (pdrtf_thread_data*) param;
    sync_wait(0, pdrtf_thread_data_ptr->ss_p);
    pdrtf_node * pdrtf_node_ptr = pdrtf_task_array[pdrtf_thread_data_ptr->pdrtf_task_id]->pdrtf_node_array[pdrtf_thread_data_ptr->pdrtf_task_node_id];

    while (1) {
        
        if (pdrtf_receive(pdrtf_node_ptr->input_data_ptr, pdrtf_node_ptr->input_data_size, pdrtf_thread_data_ptr->pdrtf_task_id, pdrtf_thread_data_ptr->pdrtf_task_node_id) < 0) {
            printf("Task %d, node %d failed reception!\n", pdrtf_thread_data_ptr->pdrtf_task_id, pdrtf_thread_data_ptr->pdrtf_task_node_id);
            break;
        }


        void (*execute)(void*) = (void*) pdrtf_node_ptr->function; /* casting the function pointer so that it can be used*/

        execute(pdrtf_thread_data_ptr);

        if (pdrtf_send(pdrtf_node_ptr->output_data_ptr, pdrtf_node_ptr->output_data_size, pdrtf_thread_data_ptr->pdrtf_task_id, pdrtf_thread_data_ptr->pdrtf_task_node_id) < 0) {
            printf("Task %d, node %d failed to send!\n", pdrtf_thread_data_ptr->pdrtf_task_id, pdrtf_thread_data_ptr->pdrtf_task_node_id);
            break;
        }
    }
    pthread_exit(0);

}

int pdrtf_launch_remote_execution(void * task_data, void* input_parameters, unsigned short input_parameters_size, unsigned short pdrtf_node_id) {
    if (pdrtf_init_state != 1) {
        return -1;
    }

    pdrtf_thread_data * pdrtf_thread_data_ptr = (pdrtf_thread_data*) task_data;

    if (pdrtf_thread_data_ptr == 0) {
        return -2;
    }

    if (pdrtf_node_id == 0 || pdrtf_node_id > get_number_of_remote_nodes()) {
        return -3;
    }

    pdrtf_node * pdrtf_node_ptr = pdrtf_task_array[pdrtf_thread_data_ptr->pdrtf_task_id]->pdrtf_node_array[pdrtf_node_id - 1];

    if (pdrtf_node_ptr->active != 1) {
        return -4;
    }

    if (input_parameters_size != pdrtf_node_ptr->input_data_size) {
        return -5;
    }
    //memmove(pdrtf_node_ptr->input_data_ptr, input_parameters, input_parameters_size);
    //pdrtf_send(pdrtf_node_ptr->input_data_ptr, pdrtf_node_ptr->input_data_size, pdrtf_thread_data_ptr->pdrtf_task_id, pdrtf_node_id - 1);
    if ((pdrtf_send(input_parameters, pdrtf_node_ptr->input_data_size, pdrtf_thread_data_ptr->pdrtf_task_id, pdrtf_node_id - 1)) < 0) {
        return -6;
    }

    return 1;

}

int pdrtf_get_remote_execution_result(void * task_data, void* output_parameters, unsigned short output_parameters_size, unsigned short pdrtf_node_id) {
    if (pdrtf_init_state != 1) {
        return -1;
    }

    pdrtf_thread_data * pdrtf_thread_data_ptr = (pdrtf_thread_data*) task_data;

    if (pdrtf_thread_data_ptr == 0) {
        return -2;
    }

    if (pdrtf_node_id == 0 || pdrtf_node_id > get_number_of_remote_nodes()) {
        return -3;
    }

    pdrtf_node * pdrtf_node_ptr = pdrtf_task_array[pdrtf_thread_data_ptr->pdrtf_task_id]->pdrtf_node_array[pdrtf_node_id - 1];

    if (pdrtf_node_ptr->active != 1) {
        return -4;
    }

    if (output_parameters_size != pdrtf_node_ptr->input_data_size) {
        return -5;
    }


    if ((pdrtf_receive(output_parameters, output_parameters_size, pdrtf_thread_data_ptr->pdrtf_task_id, pdrtf_node_id - 1)) < 0) {
        return -6;
    }

    return 1;

}

void * pdrtf_task_client_thread(void * param) {
    pdrtf_thread_data * pdrtf_thread_data_ptr = (pdrtf_thread_data*) param;
    struct timespec next, now;

    sync_wait(&next, pdrtf_thread_data_ptr->ss_p);
    pdrtf_task * pdrtf_task_ptr = pdrtf_task_array[pdrtf_thread_data_ptr->pdrtf_task_id];

    while (1) {
        timespec_add_ms(&next, pdrtf_task_ptr->period_in_ms);

        void (*execute)(void*) = (void*) pdrtf_task_ptr->function; /* casting the function pointer so that it can be used*/

        execute(pdrtf_thread_data_ptr);

        clock_gettime(CLOCK_MONOTONIC, &now);

        if (timespec_cmp(&now, &next) > 0) {
            printf("Task %d deadline failed\n", pdrtf_thread_data_ptr->pdrtf_task_id);
            break;
        } else {
            clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &next, NULL);
        }
    }
    pthread_exit(0);
}

int pdrtf_launch_tasks() {
    int ret;
    if (pdrtf_init_state != 1) {
        return -1;
    }
    if (pdrtf_launch_state != 0) {
        return -2;
    }
    pdrtf_launch_state = 1;
    if ((ret = pdrtf_open_streams()) < 0) {
        printf("pdrtf_open_streams return %d", ret);
        return -3;
    }

    unsigned short task_index;
    unsigned short number_of_active_remote_tasks = 0;
    synchronous_start_param ss_p;
    pdrtf_thread_data * pdrtf_thread_data_ptr;

    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
    pthread_attr_setschedpolicy(&attr, SCHED_FIFO);
    struct sched_param priority;

    if (get_local_node_type() < 0) {
        return-4;
    }
    if (get_local_node_type() == 1) {
        sync_init(number_of_pdrtf_tasks, &ss_p);


        for (task_index = 0; task_index < number_of_pdrtf_tasks; task_index++) {
            pdrtf_thread_data_ptr = calloc(1, sizeof (pdrtf_thread_data));
            if (pdrtf_thread_data_ptr == 0) {
                return -6;
            }
            pdrtf_thread_data_ptr->pdrtf_task_id = task_index;
            pdrtf_thread_data_ptr->pdrtf_task_node_id = 0;
            pdrtf_thread_data_ptr->ss_p = &ss_p;

            priority.sched_priority = pdrtf_task_array[task_index]->linux_priority;
            pthread_attr_setschedparam(&attr, &priority);
            if ((pthread_create(&pdrtf_task_array[task_index]->thread_id, &attr, pdrtf_task_client_thread, pdrtf_thread_data_ptr)) != 0) {
                return -7;
            }

        }
        sync_start(&ss_p);
        for (task_index = 0; task_index < number_of_pdrtf_tasks; task_index++) {


            if ((pthread_join(pdrtf_task_array[task_index]->thread_id, 0)) != 0) {
                return -8;
            }



        }

    } else {
        for (task_index = 0; task_index < number_of_pdrtf_tasks; task_index++) {
            if (pdrtf_task_array[task_index]->pdrtf_node_array[get_local_node_id()]->active == 1) {
                number_of_active_remote_tasks++;
            }
        }
        sync_init(number_of_active_remote_tasks, &ss_p);

        for (task_index = 0; task_index < number_of_pdrtf_tasks; task_index++) {

            if (pdrtf_task_array[task_index]->pdrtf_node_array[get_local_node_id()]->active == 1) {
                pdrtf_thread_data_ptr = calloc(1, sizeof (pdrtf_thread_data));

                pdrtf_thread_data_ptr->pdrtf_task_id = task_index;
                pdrtf_thread_data_ptr->pdrtf_task_node_id = get_local_node_id();
                pdrtf_thread_data_ptr->ss_p = &ss_p;


                priority.sched_priority = pdrtf_task_array[task_index]->linux_priority;
                pthread_attr_setschedparam(&attr, &priority);

                if ((pthread_create(&pdrtf_task_array[task_index]->pdrtf_node_array[get_local_node_id()]->thread_id, &attr, pdrtf_task_remote_thread, pdrtf_thread_data_ptr)) != 0) {
                    return -9;
                }
            }
        }
        sync_start(&ss_p);
        for (task_index = 0; task_index < number_of_pdrtf_tasks; task_index++) {

            if (pdrtf_task_array[task_index]->pdrtf_node_array[get_local_node_id()]->active == 1) {

                if ((pthread_join(pdrtf_task_array[task_index]->pdrtf_node_array[get_local_node_id()]->thread_id, 0)) != 0) {
                    return -10;
                }
            }
        }


    }
    if (((ret = ftt_se_wrapper_stop())) < 0) {
        print_error("ftt_se_wrapper_stop", ret);
        return -11;
    }
    clean_configuration();
    pdrtf_init_state = 0;
    pdrtf_launch_state = 0;

    return 1;
}

int pdrtf_start() {
    if (pdrtf_init_state != 0) {
        return -1;
    }
    if (pdrtf_launch_state != 0) {
        return -2;
    }

    struct sched_param priority;
    priority.sched_priority = 49;
    sched_setscheduler(0, SCHED_FIFO, &priority);

    int ret;

    if ((ret = read_configuration()) < 0) {
        print_error("read_cluster_configuration", ret);
        return -3;
    }

    char* device_name = get_node_device_name();
    if (((ret = ftt_se_wrapper_start(device_name))) < 0) {
        print_error("ftt_se_wrapper_start", ret);
        return -4;
    }
    pdrtf_init_state = 1;


    return 1;

}
