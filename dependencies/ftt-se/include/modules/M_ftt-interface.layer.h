/*****************************************************************************
 * M_ftt-interface.layer.h:
 *****************************************************************************
 * Copyright (C) 2006-2012 the FTT-SE team.
 *
 * Author: Ricardo Marau <marau at fe.up.pt>
 *
 * FTT-SE is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  FTT-SE is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with FTT-SE.  If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/

#ifndef  M_FTT_INTERFACE_LAYER_H
#define  M_FTT_INTERFACE_LAYER_H
#include "ftt-interface.layer_types.h"

/** \defgroup MASTER_FTT_INTERFACE_LAYER_H FTT interface layer - Master API 
 This is the interface for the FTT-SE interface layer, implementing the Master view of a few mechanisms that better integrate the protocol and the application. Among those, the namespace mapping of FTT channels and application message streams, remote negotiation of communication channels. 
 \image html ftt-se_InterfaceMaster.png
 \image latex ftt-se_InterfaceMaster.eps "FTT-SE Core Interface architecture" width=15cm
 */
//@{

/** Initializes the Master ftt-interface layer. \return 0 - OK */
extern signed char M_FTT_INTERFACE_L_init(void);

/** Closes the Master ftt-interface layer. \return 0 - OK */
extern signed char M_FTT_INTERFACE_L_close(void);

/* Begin application prototypes - this comment is used to replicate the API. The following will be copied. */

/** Blocks on events of Plug and Unplug. \return 0 - OK */
extern signed char M_FTT_INTERFACE_L_nodes_status(
												  unsigned char *event,     ///< [out] 1 - Plugged node; 0 - Unplugged
												  unsigned int *node_app_id ///< [out] The Node ID that was given by the remote application or assigned by the master.
												  );


#if 0
/** Negotiation request to register a new Message channel. \return 0 - OK */
extern signed char M_S_FTT_INTERFACE_L_var_add_load (
													 M_INT_APP_MSG_ID id,              ///< [in] The application given message ID.
													 unsigned int size,                ///< [in] Message size [bytes].
													 unsigned int max_size,            ///< [in] Message maximum size [bytes]. To cope with possible message adaptation.
													 unsigned int max_prod_no,         ///< [in] Message maximum number of producers.
													 unsigned short MTU,               ///< [in] Maximum transmission unit [bytes].
													 unsigned short period,            ///< [in] Message period [EC's]
													 unsigned short relative_offset    ///< [in] not used.
													 );

#endif

/* End application prototypes - this comment is used to replicate the API */

//@}


#endif
