/*****************************************************************************
 * ethernet.layer.h:
 *****************************************************************************
 * Copyright (C) 2006-2012 the FTT-SE team.
 *
 * Author: Ricardo Marau <marau at fe.up.pt>
 *
 * FTT-SE is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  FTT-SE is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with FTT-SE.  If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/

#ifndef ETHERNET_LAYER_H
#define ETHERNET_LAYER_H


/** \defgroup ETHERNET_LAYER_H Ethernet layer functions */
//@{

/** Initializes the network interface. \return 0 - OK */
extern signed char ETH_L_init(
		unsigned char dev_num,/**< [in] Selects the network device.
					\n For \p RTL \p refers to the LNet-defined device number.
					\n For \p L26 \p refers to device x in the form: ethx. */
		char *dev_name,
		unsigned char *ret_mac /**< [out] Pointer to return the MAC addres - 6 Bytes required */

		);

/** Closes the network interface. \return 0 - OK */
extern signed char ETH_L_close(void);

/** Checks if the a device has been initiated. \return 0 - if positive*/
extern signed char ETH_L_Init_check(void);

/** Registers a function that is called upon a message reception... Interrupt driven. */
extern void ETH_L_rx_fun_register(
		void *fun /**< [in] Pointer to the funtion with the following prototype:
			   * \n void fun(unsigned char *rmsg, unsigned short size, struct rtl_timespec timestamp)
			   * \n \p rmsg \p Pointer to the msg content.
			   * \n \p size \p of it.
			   * \n \p timestamp \p */
		);

/** Unregisters the function to be called when receiving packets.*/
extern void ETH_L_rx_fun_unregister(void);

/** Reserves a transmitting buffer from the device driver. \return 0 - OK*/
extern signed char ETH_L_tx_reserve_buffer(
		unsigned char **ret_buf_p /**< [out] memory pointer to where it writes the buffer pointer.*/
		);
/** Frees the last reserved buffer. \return 0 - OK*/
extern signed char ETH_L_tx_un_reserve_buffer( void );

/** Triggers the transmission of a buffer. 
  *
  * If a pre-reservation was issued, it sends that buffer and frees it.
  * If without pre-reservation then it copies and sends the contents from \p buf.
  * \note Transmissions must be fully specified. The tx'ing packet must include the Ethernet headers (Destination MAC, Source MAC, and ETH type) as well. 
  * \return 0 - OK */
extern signed char ETH_L_tx_send_messagebuffer(
		unsigned char *buf, /**< [in] Pointer to the tx'ing packet. NULL if a pre-reservation was issued. */
		unsigned short m_size /**< [in] Size in bytes of the whole packet, Ethernet headers included. */
		);

#define ETH_L_Dest_addr(_buf)	((unsigned char *)_buf+0)
#define ETH_L_Source_addr(_buf)	((unsigned char *)_buf+6)
#define ETH_L_Type_addr(_buf)	((unsigned char *)_buf+12)
#define ETH_L_Data_addr(_buf)	((unsigned char *)_buf+14)

/** Translate an Ethernet MAC address from a text string to an eth_addr structure */
extern void ETH_L_Str2Addr(unsigned char *add, unsigned char *ds);

/** Copy an Ethernet MAC address from src to dst */
extern void ETH_L_CopyMAC_to_from(unsigned char *dest, unsigned char *src);

/** Compare MAC addresses 
 * \return 1 if true, 0 if false. */
extern unsigned char ETH_L_CompareMAC(unsigned char *first, unsigned char *second);
//@}

#endif
