/*****************************************************************************
 * ftt_slave.h:
 *****************************************************************************
 * Copyright (C) 2006-2012 the FTT-SE team.
 *
 * Author: Ricardo Marau <marau at fe.up.pt>
 *
 * FTT-SE is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  FTT-SE is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with FTT-SE.  If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/



#ifndef FTT_SLAVE_H
#define FTT_SLAVE_H

extern signed char ftt_slave_start_up(unsigned char *mac, unsigned char dev_num, char *dev_name, unsigned char app_nodeID);

extern void ftt_slave_close_up(void);

#include "./modules/ftt-interface.layer_types.h"

/** Blocks the application thread execution, wating for the FTT-slave communication establishment.
 *  \details This must be called after the initialization and before any calling any FTT-related function.
 *   It blocks until the interface layer is fully initialized. Notice that after S_FTT_INTERFACE_L_init 
 *   the system might still not be fully initialized 
 *  \return The app_id */
extern signed int S_FTT_INTERFACE_L_init_block(unsigned int *ret_ec_len_us);

/** Registers the thread as consumer of an application Message ID. Blocks until that Message ID appears registered in the running node ID. It waits for all distributed entities registration. \return 0 - OK */
extern signed char S_FTT_INTERFACE_L_attach_rx(
		M_INT_APP_MSG_ID app_id ///< [in] The application given message ID.
		);

/** Registers the thread as producer of an application Message ID. Blocks until that Message ID appears registered in the running node ID. It waits for all distributed entities registration. \return 0 - OK */
extern signed char S_FTT_INTERFACE_L_attach_tx( 
		M_INT_APP_MSG_ID app_id ///< [in] The application given message ID.
		);

extern signed char S_FTT_INTERFACE_L_attach_details( M_INT_APP_MSG_ID app_id );

/** Unregisters the thread as producer/consumer. Blocks for the reply. \return 0 - OK */
extern signed char S_FTT_INTERFACE_L_dettach( M_INT_APP_MSG_ID app_id );


/** Binds the thread to an existing application Message ID. \return 0 - OK */
extern signed char S_FTT_INTERFACE_L_bind( 
		M_INT_APP_MSG_ID app_id, ///< [in] The application given message ID.
		void *mem,               ///< [in] Pointer to the memory used to handle the variable. If NULL, use internal memory

		unsigned int mem_size, ///< [in] The size of the given memory heap.
		unsigned char  slots_no  ///< [in] Number of memory slots to be used. For a SM it must be 3. For an AM it may range from 1 to 255.
		);
#warning test AMs with 1 slot fifo

/** Binds the thread to an existing application Message ID. \return 0 - OK */
extern signed char S_FTT_INTERFACE_L_unbind(
		M_INT_APP_MSG_ID app_id  ///< [in] The application given message ID.
		);

/** Checks wheather the message is bind-ed */
extern signed char S_FTT_INTERFACE_L_isbind(
		M_INT_APP_MSG_ID app_id  ///< [in] The application given message ID.
		);

/** Requests the actual properties of an executing message channel. \return The current tag of app_id*/
extern signed short S_FTT_INTERFACE_L_var_details( 
		M_INT_APP_MSG_ID app_id,            ///< [in] The application given message ID.
		unsigned int *current_var_size,      ///< [out] Pointer where to return the message size.
		unsigned short *current_var_period   ///< [out] Pointer where to return the message period.
		);

/** Reserves a memory slot to prepare a message transmission \returns 0 - OK */
extern signed char S_FTT_INTERFACE_L_pre_send( 
		M_INT_APP_MSG_ID app_id, ///< [in] The application given message ID.
		unsigned char block_on_reservation, ///< [in] Flag (boolean) to block on an AM memory slot.
		void **mem               ///< [out] Pointer where to return the pointer to the memory slot.
		);
/** Aborts the pre-transmission procedure \returns 0 - OK */
extern signed char S_FTT_INTERFACE_L_pre_send_abort( 
		M_INT_APP_MSG_ID app_id ///< [in] The application given message ID.
		);
/** Trigger the FTT transmission of the message \returns 0 - OK */
extern signed char S_FTT_INTERFACE_L_send ( 
		M_INT_APP_MSG_ID app_id,            ///< [in] The application given message ID.
		unsigned char tag,                  ///< [in] The current tag with which data was produced. This signature is matched to the current channel tag.
		void *mem,                          ///< [in] Pointer to the message location. NULL if coming from S_FTT_INTERFACE_L_pre_send of that message.
		unsigned char block_on_reservation, ///< [in] Flag (boolean) to block on an AM memory slot.
		unsigned char block_on_tx           ///< [in] Flag (boolean) whether to block for the actual message transmission in the network. To notice that if mixing non-blocking and blocking the latter may occur even when a non-blocking tx is dispatched.
		);

/** Reserves a memory slot to read a received message slot \returns 0 - OK */
extern signed char S_FTT_INTERFACE_L_pre_receive ( 
		M_INT_APP_MSG_ID app_id, ///< [in] The application given message ID.
		unsigned char *ret_tag,  ///< [out] Pointer where to return the tag with which the content was received.
		void **mem,              ///< [out] Pointer where to return the Pointer to the message slot.
		RX_FLAGS flags           ///< [in] Flag whether to non-block, block on new local reception or block on new data in the sender (applicable for synchronous message channels).
		);

/** Receives the message. If called after S_FTT_INTERFACE_L_pre_receive, just frees the memory slot. \returns 0 - OK */
extern signed char S_FTT_INTERFACE_L_receive( 
		M_INT_APP_MSG_ID app_id, ///< [in] The application given message ID.
		unsigned char *ret_tag,  ///< [out] Pointer where to return the tag with which the content was received. NULL if coming from S_FTT_INTERFACE_L_pre_receive
		void *mem,               ///< [out] Pointer where to copy the received message. NULL if coming from S_FTT_INTERFACE_L_pre_receive.
		RX_FLAGS flags           ///< [in] Flag whether to non-block, block on new local reception or block on new data in the sender (applicable for synchronous message channels).
		);

/** Block on a Message channel activity. \return 0 - OK */
extern signed char S_FTT_INTERFACE_L_block ( 
		M_INT_APP_MSG_ID app_id  ///< [in] The application given message ID.
		);

//Stream Negotiation

extern signed char S_FTT_INTERFACE_L_var_add_full ( 
		M_INT_APP_MSG_ID id,
		unsigned int size,
		unsigned int max_size,
		unsigned char max_prod_no,
		unsigned short MTU,
		unsigned short period,
		unsigned char sender_ftt_app_id,
		unsigned char receiver_ftt_app_id
		);

extern signed char S_FTT_INTERFACE_L_var_rm_full ( M_INT_APP_MSG_ID id	);

/** Negotiation request to register a new Message channel. \return 0 - OK */
extern signed char S_FTT_INTERFACE_L_var_add_load ( 
		M_INT_APP_MSG_ID id,              ///< [in] The application given message ID.
		unsigned int size,                ///< [in] Message size [bytes].
		unsigned int max_size,            ///< [in] Message maximum size [bytes]. To cope with possible message adaptation.
		unsigned char max_prod_no,        ///< [in] Message maximum producers no. Implications on the memory size required for the variable. Not fully tested. Use 1 for now!
		unsigned short MTU,               ///< [in] Maximum transmission unit [bytes].
		unsigned short period,            ///< [in] Message period [EC's]
		unsigned short relative_offset    ///< [in] not used.
		);

extern signed char S_FTT_INTERFACE_L_var_rm_load ( M_INT_APP_MSG_ID id	);

/** Negotiation request to modify the period of a Message channel. \return 0 - OK */
extern signed char S_FTT_INTERFACE_L_var_req_change_period ( 
		M_INT_APP_MSG_ID id,              ///< [in] The application given message ID.
		unsigned short period             ///< [in] Message period [EC's]
		);

/** Negotiation request to modify the QoS properties of a Message channel. \return 0 - OK */
extern signed char S_FTT_INTERFACE_L_var_req_change_qos ( 
		M_INT_APP_MSG_ID id,              ///< [in] The application given message ID.
		signed short prio,                ///< [in] QoS priority.
		unsigned char pairs_no,           ///< [in] Number of valid operacional pairs.
		S_FTT_INTERFACE_L_QOS_PAIR *pairs ///< [in] Pointer to array of pairs of valid operational results.
		);

/** Negotiation request to modify the QoS priority of a Message channel. \return 0 - OK */
extern signed char S_FTT_INTERFACE_L_var_req_change_qos_prio ( M_INT_APP_MSG_ID id, unsigned short prio );

extern signed char S_FTT_INTERFACE_L_nodes_status( unsigned char *event, unsigned int *node_app_id );




#endif
