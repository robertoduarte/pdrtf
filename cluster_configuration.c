
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <sys/ioctl.h>

#include <netinet/in.h>

#include <linux/if.h>
#include <arpa/inet.h>
#include <string.h>
#include <libconfig.h>

#include "cluster_configuration.h"



#define CONFIGURATION_FILE  "config.cfg"

char * device_name;
unsigned char configuration_init_state = 0;
unsigned short local_node_type = 0;
unsigned short local_node_id = 0;
unsigned short number_of_remote_nodes = 0;

int check_device_address(char * device_name, char * mac_address) {

    /* This function creates a socket and uses it to find
     * the mac address of the eth0 interface and compares it with the ones
     * in the array of structures "nodes_addresses" of type "node_mac_address".
     * If there is a match the position of the array is returned else
     * "-1" is returned.*/

    int fd, ret;
    struct ifreq ifr;

    if ((fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
        print_error("socket", fd);
        return -1;
    }

    ifr.ifr_addr.sa_family = AF_INET;

    strncpy((char *) ifr.ifr_name, device_name, IFNAMSIZ - 1);

    if ((ret = ioctl(fd, SIOCGIFHWADDR, &ifr)) < 0) {
        print_error("ioctl", ret);
        return -2;
    }

    if ((ret = close(fd)) < 0) {
        print_error("close", ret);
        return -3;
    }

    unsigned char * temp_mac_address = (unsigned char *) ifr.ifr_hwaddr.sa_data;

    char mac_address_string[17];

    if ((ret = snprintf(mac_address_string, 18, "%.2x:%.2x:%.2x:%.2x:%.2x:%.2x", temp_mac_address[0], temp_mac_address[1], temp_mac_address[2], temp_mac_address[3], temp_mac_address[4], temp_mac_address[5])) < 0) {
        print_error("snprintf", ret);
        return -4;
    }


    if (strcmp(mac_address, (char *) mac_address_string) == 0) {
        return 1;
    }
    return 0;

}

int read_configuration() {

    if (configuration_init_state == 0) {
        const char * temp_mac_address;
        const char * temp_device_name;

        config_t libconfig_cfg;
        config_setting_t *libconfig_cfg_element, *libconfig_cfg_sub_element;

        config_init(&libconfig_cfg);

        if (!config_read_file(&libconfig_cfg, CONFIGURATION_FILE)) {
            /* If configuration file does not exist it returns 0*/
            config_destroy(&libconfig_cfg);
            print_error("config_read_file", 0);
            return -1;
        } else {
            config_lookup_string(&libconfig_cfg, "local_node_mac_address", &temp_mac_address);
            config_lookup_string(&libconfig_cfg, "local_node_device_name", &temp_device_name);
            if (check_device_address((char*) temp_device_name, (char*) temp_mac_address) == 1) {
                local_node_type = 1;
                device_name = calloc(1, strlen(temp_device_name));
                memmove(device_name, temp_device_name, strlen(temp_device_name));
            }

            libconfig_cfg_element = config_lookup(&libconfig_cfg, "remote_nodes"); /* Gets all configuration elements within the element "nodes" */

            number_of_remote_nodes = config_setting_length(libconfig_cfg_element); /* Counting the number of elements*/

            if (local_node_type != 1 && number_of_remote_nodes != 0) {
                local_node_type = 2;
                int i;
                for (i = 0; i < number_of_remote_nodes; i++) {
                    /* The order of the nodes is their id so the addresses are stored with that position in the array*/

                    libconfig_cfg_sub_element = config_setting_get_elem(libconfig_cfg_element, i);

                    config_setting_lookup_string(libconfig_cfg_sub_element, "mac_address", &temp_mac_address); /* Getting the mac address from that position to the temp variable*/
                    config_setting_lookup_string(libconfig_cfg_sub_element, "device_name", &temp_device_name); /* Getting the mac address from that position to the temp variable*/


                    if (check_device_address((char*) temp_device_name, (char*) temp_mac_address) == 1) {
                        local_node_id = i;
                        device_name = calloc(1, strlen(temp_device_name));
                        memmove(device_name, temp_device_name, strlen(temp_device_name));
                        break;
                    }

                }
            }

            config_destroy(&libconfig_cfg); /* Closing up the libconf library.*/
        }
        configuration_init_state = 1;
        return 1;
    } else {
        return 0;
    }
}

void clean_configuration() {
    free(device_name);
    configuration_init_state = 0;
    local_node_type = 0;
    local_node_id = 0;
    number_of_remote_nodes = 0;
}

int get_local_node_type() {
    if (configuration_init_state != 1) {
        return -1;
    }
    return local_node_type;

}

char * get_node_device_name() {
    if (configuration_init_state != 1) {
        return 0;
    }
    return device_name;
}

int get_local_node_id() {
    if (configuration_init_state != 1) {
        return -1;
    }
    if (local_node_type != 2) {
        return -2;
    }
    if (number_of_remote_nodes == 0) {
        return -3;
    }
    return local_node_id;

}

int get_number_of_remote_nodes() {
    if (configuration_init_state != 1) {
        return -1;
    }
    return number_of_remote_nodes;
}
