#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <pthread.h>
#include <stdint.h>
#include <math.h>
#include "real_time_utils.h"
#include "pdrtf.h"

#define WORKLOAD_CALIBRATION

#define TEST_ITERATIONS 10
#define TASK_1_WORKLOAD 41300000
#define TASK_2_WORKLOAD 69700000
long int task_1_time_results[2][TEST_ITERATIONS];
long int task_2_time_results[2][TEST_ITERATIONS / 2];
FILE * fp;
FILE * fp_2;

void waste_cpu(int iterations) {
    register int iter = iterations;
    while (iter--);
}



int workload_calibration_test(char * log_file_name, int calibration_wect_in_ms, int work_load_calibration, unsigned char ignore_deadline) {
    struct timespec start, now, deadline, elapsed;

    struct sched_param priority;
    priority.sched_priority = 51;
    sched_setscheduler(0, SCHED_FIFO, &priority);
    volatile int test_counter = TEST_ITERATIONS;


    unsigned char deadline_failed = 0;
    while (test_counter--) {
        clock_gettime(CLOCK_MONOTONIC, &start);
        deadline.tv_nsec = start.tv_nsec;
        deadline.tv_sec = start.tv_sec;
        timespec_add_ms(&deadline, calibration_wect_in_ms);

        waste_cpu(work_load_calibration);

        clock_gettime(CLOCK_MONOTONIC, &now);

        if (timespec_cmp(&now, &deadline) > 0 && ignore_deadline != 1) {
            printf("Deadline failed! Calibration value: %d.\n", work_load_calibration);
            timespec_sub(&elapsed, &now, &start);
            printf("Execution time: %ld.%.9ld\n", (long int) elapsed.tv_sec, elapsed.tv_nsec);
            deadline_failed = 1;
            break;
        } else {
            timespec_sub(&elapsed, &now, &start);
            task_1_time_results[0][(TEST_ITERATIONS - 1) - test_counter] = (long int) elapsed.tv_sec;
            task_1_time_results[1][(TEST_ITERATIONS - 1) - test_counter] = elapsed.tv_nsec;

        }

    }
    if (deadline_failed) {

        return -1;
    }
    FILE * fp;

    fp = fopen(log_file_name, "w+");
    int i = 0;
    printf("Successfull workload calibration value: %d.\n", work_load_calibration);
    fprintf(fp, "Workload calibration value, %d\n", work_load_calibration);
    while (i < TEST_ITERATIONS) {

        if (task_1_time_results[0][i] == 0 && task_1_time_results[1][i] == 0) {
            break;
        }
        fprintf(fp, "%ld.%.9ld\n", task_1_time_results[0][i], task_1_time_results[1][i]);

        i++;
    }
    fclose(fp);
    return 1;

}

void workload_calibration(char * log_file_name, int test_iterations, int calibration_wect_in_ms, int work_load_calibration_start_value, int workload_calibration_step_size) {
    int current_work_load_calibration_value = work_load_calibration_start_value;

    while (workload_calibration_test(log_file_name, calibration_wect_in_ms, current_work_load_calibration_value, 0) != 1) {
        current_work_load_calibration_value = current_work_load_calibration_value - workload_calibration_step_size;
    }
}

void * task_1(void * param) {
    static int test_iterations = TEST_ITERATIONS;
    struct timespec start, end, elapsed;
    clock_gettime(CLOCK_MONOTONIC, &start);
    char input[1000] = "Input test!";
    int ret = 0;
    unsigned char exit = 0;
    if ((ret = pdrtf_launch_remote_execution(param, &input, sizeof (input), 1)) < 0) {
        printf(" Task 1 pdrtf_launch_remote_execution returned %d\n", ret);
        exit = 1;
    }

    if ((ret = pdrtf_launch_remote_execution(param, &input, sizeof (input), 2)) < 0) {
        printf(" Task 1 pdrtf_launch_remote_execution returned %d\n", ret);
        exit = 1;
    }
    if ((ret = pdrtf_launch_remote_execution(param, &input, sizeof (input), 3)) < 0) {
        printf(" Task 1 pdrtf_launch_remote_execution returned %d\n", ret);
        exit = 1;
    }

    waste_cpu(TASK_1_WORKLOAD / 4);

    char output1[1000];
    char output2[1000];
    char output3[1000];

    if ((ret = pdrtf_get_remote_execution_result(param, &output1, sizeof (output1), 1)) < 0) {
        printf(" Task 1 pdrtf_get_remote_execution_result returned %d\n", ret);
        exit = 1;
    }

    if ((ret = pdrtf_get_remote_execution_result(param, &output2, sizeof (output2), 2)) < 0) {
        printf(" Task 1 pdrtf_get_remote_execution_result returned %d\n", ret);
        exit = 1;
    }
    if ((ret = pdrtf_get_remote_execution_result(param, &output3, sizeof (output3), 3)) < 0) {
        printf(" Task 1 pdrtf_get_remote_execution_result returned %d\n", ret);
        exit = 1;
    }

    test_iterations--;
    clock_gettime(CLOCK_MONOTONIC, &end);
    timespec_sub(&elapsed, &end, &start);
    task_1_time_results[0][(TEST_ITERATIONS - 1) - test_iterations] = (long int) elapsed.tv_sec;
    task_1_time_results[1][(TEST_ITERATIONS - 1) - test_iterations] = elapsed.tv_nsec;

    if (test_iterations == 0 || exit == 1) {
        pthread_exit(0);
    }
    return 0;
}

void * task_1_sub_task(void * param) {
    char input[1000];
    pdrtf_get_input_parameters(param, &input, sizeof (input));

    waste_cpu(TASK_1_WORKLOAD / 4);
    char output[1000] = "Output test!";
    pdrtf_set_output_parameters(param, &output, sizeof (output));
    return 0;
}

void * task_2(void * param) {
    static int test_iterations = TEST_ITERATIONS / 2;
    struct timespec start, end, elapsed;
    clock_gettime(CLOCK_MONOTONIC, &start);
    char input[1000] = "Input test!";
    int ret = 0;
    unsigned char exit = 0;
    if ((ret = pdrtf_launch_remote_execution(param, &input, sizeof (input), 1)) < 0) {
        printf(" Task 2 pdrtf_launch_remote_execution returned %d\n", ret);
        exit = 1;
    }

    if ((ret = pdrtf_launch_remote_execution(param, &input, sizeof (input), 2)) < 0) {
        printf(" Task 2 pdrtf_launch_remote_execution returned %d\n", ret);
        exit = 1;
    }
    if ((ret = pdrtf_launch_remote_execution(param, &input, sizeof (input), 3)) < 0) {
        printf(" Task 2 pdrtf_launch_remote_execution returned %d\n", ret);
        exit = 1;
    }

    waste_cpu(TASK_2_WORKLOAD / 4);

    char output1[1000];
    char output2[1000];
    char output3[1000];

    if ((ret = pdrtf_get_remote_execution_result(param, &output1, sizeof (output1), 1)) < 0) {
        printf(" Task 2 pdrtf_get_remote_execution_result returned %d\n", ret);
        exit = 1;
    }

    if ((ret = pdrtf_get_remote_execution_result(param, &output2, sizeof (output2), 2)) < 0) {
        printf(" Task 2 pdrtf_get_remote_execution_result returned %d\n", ret);
        exit = 1;
    }
    if ((ret = pdrtf_get_remote_execution_result(param, &output3, sizeof (output3), 3)) < 0) {
        printf(" Task 2 pdrtf_get_remote_execution_result returned %d\n", ret);
        exit = 1;
    }

    test_iterations--;
    clock_gettime(CLOCK_MONOTONIC, &end);
    timespec_sub(&elapsed, &end, &start);
    task_2_time_results[0][(TEST_ITERATIONS / 2 - 1) - test_iterations] = (long int) elapsed.tv_sec;
    task_2_time_results[1][(TEST_ITERATIONS / 2 - 1) - test_iterations] = elapsed.tv_nsec;

    if (test_iterations == 0 || exit == 1) {
        pthread_exit(0);
    }
    return 0;
}

void * task_2_sub_task(void * param) {
    char input[1000];
    pdrtf_get_input_parameters(param, &input, sizeof (input));

    waste_cpu(TASK_2_WORKLOAD / 4);
    char output[1000] = "Output test!";
    pdrtf_set_output_parameters(param, &output, sizeof (output));
    return 0;
}

int main() {
#ifdef  WORKLOAD_CALIBRATION


    workload_calibration("calibration_test_task_1.csv", 10, 200, 213000000, 100000);
    //workload_calibration("calibration_test_task_2.csv", 1000, 300, 69700000, 100000);

#else
    pdrtf_start();
    int node_type = 0;
    node_type = pdrtf_get_local_node_type();

    if (node_type == 1) {
        //fp = fopen("4_nodes_distributed_task_1_alone_results.csv", "w+");
        fp_2 = fopen("4_nodes_distributed_task_2_alone_results.csv", "w+");
    }
    /*

        int pdrtf_task_1 = new_pdrtf_task(task_1, 150, 48);
        set_pdrtf_task_sub_task(pdrtf_task_1, 1, task_2_sub_task, 1000, 1000);
        set_pdrtf_task_sub_task(pdrtf_task_1, 2, task_2_sub_task, 1000, 1000);
        set_pdrtf_task_sub_task(pdrtf_task_1, 3, task_2_sub_task, 1000, 1000);
     */

    int pdrtf_task_2 = new_pdrtf_task(task_2, 300, 47);
    set_pdrtf_task_sub_task(pdrtf_task_2, 1, task_2_sub_task, 1000, 1000);
    set_pdrtf_task_sub_task(pdrtf_task_2, 2, task_2_sub_task, 1000, 1000);
    set_pdrtf_task_sub_task(pdrtf_task_2, 3, task_2_sub_task, 1000, 1000);

    pdrtf_launch_tasks();
    if (node_type == 1) {
        int i;
        /*
                for (i = 0; i < TEST_ITERATIONS; i++) {
                    fprintf(fp, "Pass %d, %ld.%.9ld", i, task_1_time_results[0][i], task_1_time_results[1][i]);
                    fprintf(fp, ",\n");
                }
         */

        for (i = 0; i < TEST_ITERATIONS / 2; i++) {
            fprintf(fp_2, "Pass %d, %ld.%.9ld", i, task_2_time_results[0][i], task_2_time_results[1][i]);
            fprintf(fp_2, ",\n");
        }
        /*

                fclose(fp);
         */

        fclose(fp_2);

    }

#endif

    return 1;
}