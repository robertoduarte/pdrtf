#ifndef CLUSTER_CONFIGURATION_H
#define	CLUSTER_CONFIGURATION_H


int read_configuration();

void clean_configuration();

int get_local_node_type();

char * get_node_device_name();

int get_local_node_id();

int get_number_of_remote_nodes();

#endif	/* CLUSTER_CONFIGURATION_H */

