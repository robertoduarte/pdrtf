#include <stdio.h>

#include "error_printer.h"


void print_error(char * function_name, int return_value){
    printf("\nError. Function %s() returned with value: %d.",function_name, return_value);
}
