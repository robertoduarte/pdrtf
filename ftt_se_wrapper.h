#ifndef FTT_SE_SLAVE_WRAPPER_H
#define	FTT_SE_SLAVE_WRAPPER_H

typedef struct stream stream;

extern int ftt_se_wrapper_new_stream(stream ** stream_ptr, unsigned short msg_id, unsigned short sync_type, unsigned char fifo_size);

extern int ftt_se_wrapper_start(char *device_name);

extern int ftt_se_wrapper_stop();

extern int ftt_se_wrapper_close_stream(stream * stream_ptr);

extern int ftt_se_wrapper_retag_stream(stream * stream_ptr);

extern int ftt_se_wrapper_rebind_stream(stream * stream_ptr);

extern int ftt_se_wrapper_initialize_producer(stream * stream_ptr);

extern int ftt_se_wrapper_initialize_consumer(stream * stream_ptr);

extern int ftt_se_wrapper_set_stream_parameters(stream * stream_ptr, unsigned int msg_size, unsigned short mtu_size, unsigned short period_in_ecs);

extern int ftt_se_wrapper_send(stream * stream_ptr, void * msg_ptr, unsigned int msg_size, unsigned short blocking_flag);

extern int ftt_se_wrapper_receive(stream * stream_ptr, void * msg_ptr, unsigned int msg_size);

#endif	/* FTT_SE_SLAVE_WRAPPER_H */
