#ifndef PDRTF_H
#define	PDRTF_H

int pdrtf_start();

int pdrtf_get_local_node_type();

int new_pdrtf_task(void * function_ptr, unsigned short period_in_ms, unsigned short linux_priority);

int set_pdrtf_task_sub_task(unsigned short pdrtf_task_id, unsigned short node_id, void * function, unsigned short input_data_size, unsigned short output_data_size);

int pdrtf_launch_tasks();

int pdrtf_launch_remote_execution(void * task_data, void* input_parameters, unsigned short input_parameters_size, unsigned short pdrtf_node_id);

int pdrtf_get_remote_execution_result(void * task_data, void* output_parameters, unsigned short output_parameters_size, unsigned short pdrtf_node_id);

int pdrtf_get_input_parameters(void * task_data, void * data, unsigned short data_size);

int pdrtf_set_output_parameters(void * task_data, void * data, unsigned short data_size);



#endif	/* PDRTF_H */
